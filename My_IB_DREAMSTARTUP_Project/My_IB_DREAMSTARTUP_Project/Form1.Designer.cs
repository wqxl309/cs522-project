﻿namespace My_IB_DREAMSTARTUP_Project
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.Connect_button = new System.Windows.Forms.Button();
            this.Message_textBox = new System.Windows.Forms.TextBox();
            this.Contract_groupBox = new System.Windows.Forms.GroupBox();
            this.Stop_Ticker_button = new System.Windows.Forms.Button();
            this.AddTicker_button = new System.Windows.Forms.Button();
            this.Exchange_textBox = new System.Windows.Forms.TextBox();
            this.Currency_textBox = new System.Windows.Forms.TextBox();
            this.SecType_textBox = new System.Windows.Forms.TextBox();
            this.Symbol_textBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Ticker_Info_groupBox = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Volume_textBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LastQty_textBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LastPx_textBox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.AskQty_textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.AskPx_textBox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.BidPx_textBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.BidQty_textBox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.State_textBox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.EWMA_textBox = new System.Windows.Forms.TextBox();
            this.Px_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.startAlgo_button = new System.Windows.Forms.Button();
            this.Contract_groupBox.SuspendLayout();
            this.Ticker_Info_groupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Px_chart)).BeginInit();
            this.SuspendLayout();
            // 
            // Connect_button
            // 
            this.Connect_button.Location = new System.Drawing.Point(12, 25);
            this.Connect_button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Connect_button.Name = "Connect_button";
            this.Connect_button.Size = new System.Drawing.Size(109, 69);
            this.Connect_button.TabIndex = 0;
            this.Connect_button.Text = "CONNECT";
            this.Connect_button.UseVisualStyleBackColor = true;
            this.Connect_button.Click += new System.EventHandler(this.Connect_button_Click);
            // 
            // Message_textBox
            // 
            this.Message_textBox.Location = new System.Drawing.Point(252, 25);
            this.Message_textBox.Multiline = true;
            this.Message_textBox.Name = "Message_textBox";
            this.Message_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Message_textBox.Size = new System.Drawing.Size(542, 274);
            this.Message_textBox.TabIndex = 1;
            // 
            // Contract_groupBox
            // 
            this.Contract_groupBox.Controls.Add(this.Stop_Ticker_button);
            this.Contract_groupBox.Controls.Add(this.AddTicker_button);
            this.Contract_groupBox.Controls.Add(this.Exchange_textBox);
            this.Contract_groupBox.Controls.Add(this.Currency_textBox);
            this.Contract_groupBox.Controls.Add(this.SecType_textBox);
            this.Contract_groupBox.Controls.Add(this.Symbol_textBox);
            this.Contract_groupBox.Controls.Add(this.label4);
            this.Contract_groupBox.Controls.Add(this.label3);
            this.Contract_groupBox.Controls.Add(this.label2);
            this.Contract_groupBox.Controls.Add(this.label1);
            this.Contract_groupBox.Location = new System.Drawing.Point(12, 101);
            this.Contract_groupBox.Name = "Contract_groupBox";
            this.Contract_groupBox.Size = new System.Drawing.Size(234, 198);
            this.Contract_groupBox.TabIndex = 2;
            this.Contract_groupBox.TabStop = false;
            this.Contract_groupBox.Text = "Contract Info";
            // 
            // Stop_Ticker_button
            // 
            this.Stop_Ticker_button.Location = new System.Drawing.Point(120, 163);
            this.Stop_Ticker_button.Name = "Stop_Ticker_button";
            this.Stop_Ticker_button.Size = new System.Drawing.Size(103, 29);
            this.Stop_Ticker_button.TabIndex = 9;
            this.Stop_Ticker_button.Text = "Stop Ticker";
            this.Stop_Ticker_button.UseVisualStyleBackColor = true;
            this.Stop_Ticker_button.Click += new System.EventHandler(this.Stop_Ticker_button_Click);
            // 
            // AddTicker_button
            // 
            this.AddTicker_button.Location = new System.Drawing.Point(6, 163);
            this.AddTicker_button.Name = "AddTicker_button";
            this.AddTicker_button.Size = new System.Drawing.Size(103, 29);
            this.AddTicker_button.TabIndex = 8;
            this.AddTicker_button.Text = "Add Ticker";
            this.AddTicker_button.UseVisualStyleBackColor = true;
            this.AddTicker_button.Click += new System.EventHandler(this.AddTicker_button_Click);
            // 
            // Exchange_textBox
            // 
            this.Exchange_textBox.Location = new System.Drawing.Point(120, 119);
            this.Exchange_textBox.Name = "Exchange_textBox";
            this.Exchange_textBox.Size = new System.Drawing.Size(75, 23);
            this.Exchange_textBox.TabIndex = 7;
            this.Exchange_textBox.Text = "SMART";
            // 
            // Currency_textBox
            // 
            this.Currency_textBox.AllowDrop = true;
            this.Currency_textBox.Location = new System.Drawing.Point(120, 90);
            this.Currency_textBox.Name = "Currency_textBox";
            this.Currency_textBox.Size = new System.Drawing.Size(75, 23);
            this.Currency_textBox.TabIndex = 6;
            this.Currency_textBox.Text = "USD";
            // 
            // SecType_textBox
            // 
            this.SecType_textBox.AllowDrop = true;
            this.SecType_textBox.Location = new System.Drawing.Point(120, 61);
            this.SecType_textBox.Name = "SecType_textBox";
            this.SecType_textBox.Size = new System.Drawing.Size(75, 23);
            this.SecType_textBox.TabIndex = 5;
            this.SecType_textBox.Text = "STK";
            // 
            // Symbol_textBox
            // 
            this.Symbol_textBox.Location = new System.Drawing.Point(120, 32);
            this.Symbol_textBox.Name = "Symbol_textBox";
            this.Symbol_textBox.Size = new System.Drawing.Size(75, 23);
            this.Symbol_textBox.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "Exchange";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Currency";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "SecType";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Symbol";
            // 
            // Ticker_Info_groupBox
            // 
            this.Ticker_Info_groupBox.Controls.Add(this.label12);
            this.Ticker_Info_groupBox.Controls.Add(this.Volume_textBox);
            this.Ticker_Info_groupBox.Controls.Add(this.label7);
            this.Ticker_Info_groupBox.Controls.Add(this.label6);
            this.Ticker_Info_groupBox.Controls.Add(this.LastQty_textBox);
            this.Ticker_Info_groupBox.Controls.Add(this.label5);
            this.Ticker_Info_groupBox.Controls.Add(this.LastPx_textBox);
            this.Ticker_Info_groupBox.Controls.Add(this.label8);
            this.Ticker_Info_groupBox.Controls.Add(this.AskQty_textBox);
            this.Ticker_Info_groupBox.Controls.Add(this.label9);
            this.Ticker_Info_groupBox.Controls.Add(this.AskPx_textBox);
            this.Ticker_Info_groupBox.Controls.Add(this.label10);
            this.Ticker_Info_groupBox.Controls.Add(this.BidPx_textBox);
            this.Ticker_Info_groupBox.Controls.Add(this.label11);
            this.Ticker_Info_groupBox.Controls.Add(this.BidQty_textBox);
            this.Ticker_Info_groupBox.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Ticker_Info_groupBox.Location = new System.Drawing.Point(12, 305);
            this.Ticker_Info_groupBox.Name = "Ticker_Info_groupBox";
            this.Ticker_Info_groupBox.Size = new System.Drawing.Size(782, 192);
            this.Ticker_Info_groupBox.TabIndex = 4;
            this.Ticker_Info_groupBox.TabStop = false;
            this.Ticker_Info_groupBox.Text = "Ticker Info";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(680, 29);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 18);
            this.label12.TabIndex = 28;
            this.label12.Text = "VOLUME";
            // 
            // Volume_textBox
            // 
            this.Volume_textBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Volume_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Volume_textBox.Location = new System.Drawing.Point(665, 50);
            this.Volume_textBox.Name = "Volume_textBox";
            this.Volume_textBox.Size = new System.Drawing.Size(100, 26);
            this.Volume_textBox.TabIndex = 27;
            this.Volume_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(237, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 18);
            this.label7.TabIndex = 26;
            this.label7.Text = "ASK PRICE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(561, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 18);
            this.label6.TabIndex = 25;
            this.label6.Text = "LAST QTY";
            // 
            // LastQty_textBox
            // 
            this.LastQty_textBox.BackColor = System.Drawing.Color.Honeydew;
            this.LastQty_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastQty_textBox.Location = new System.Drawing.Point(550, 51);
            this.LastQty_textBox.Name = "LastQty_textBox";
            this.LastQty_textBox.Size = new System.Drawing.Size(100, 26);
            this.LastQty_textBox.TabIndex = 24;
            this.LastQty_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(449, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 18);
            this.label5.TabIndex = 23;
            this.label5.Text = "LAST PRICE";
            // 
            // LastPx_textBox
            // 
            this.LastPx_textBox.BackColor = System.Drawing.Color.Honeydew;
            this.LastPx_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LastPx_textBox.Location = new System.Drawing.Point(444, 51);
            this.LastPx_textBox.Name = "LastPx_textBox";
            this.LastPx_textBox.Size = new System.Drawing.Size(100, 26);
            this.LastPx_textBox.TabIndex = 22;
            this.LastPx_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(350, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 18);
            this.label8.TabIndex = 21;
            this.label8.Text = "ASK QTY";
            // 
            // AskQty_textBox
            // 
            this.AskQty_textBox.BackColor = System.Drawing.Color.MistyRose;
            this.AskQty_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AskQty_textBox.Location = new System.Drawing.Point(334, 51);
            this.AskQty_textBox.Name = "AskQty_textBox";
            this.AskQty_textBox.Size = new System.Drawing.Size(100, 26);
            this.AskQty_textBox.TabIndex = 20;
            this.AskQty_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(232, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 18);
            this.label9.TabIndex = 19;
            this.label9.Text = " ";
            // 
            // AskPx_textBox
            // 
            this.AskPx_textBox.BackColor = System.Drawing.Color.MistyRose;
            this.AskPx_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AskPx_textBox.Location = new System.Drawing.Point(228, 51);
            this.AskPx_textBox.Name = "AskPx_textBox";
            this.AskPx_textBox.Size = new System.Drawing.Size(100, 26);
            this.AskPx_textBox.TabIndex = 18;
            this.AskPx_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(132, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "BID PRICE";
            // 
            // BidPx_textBox
            // 
            this.BidPx_textBox.BackColor = System.Drawing.Color.LightCyan;
            this.BidPx_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BidPx_textBox.Location = new System.Drawing.Point(122, 51);
            this.BidPx_textBox.Name = "BidPx_textBox";
            this.BidPx_textBox.Size = new System.Drawing.Size(100, 26);
            this.BidPx_textBox.TabIndex = 16;
            this.BidPx_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(36, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(66, 18);
            this.label11.TabIndex = 15;
            this.label11.Text = "BID QTY";
            // 
            // BidQty_textBox
            // 
            this.BidQty_textBox.BackColor = System.Drawing.Color.LightCyan;
            this.BidQty_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BidQty_textBox.Location = new System.Drawing.Point(16, 51);
            this.BidQty_textBox.Name = "BidQty_textBox";
            this.BidQty_textBox.Size = new System.Drawing.Size(100, 26);
            this.BidQty_textBox.TabIndex = 14;
            this.BidQty_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1127, 429);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 18);
            this.label13.TabIndex = 32;
            this.label13.Text = "State";
            this.label13.Visible = false;
            // 
            // State_textBox
            // 
            this.State_textBox.BackColor = System.Drawing.Color.Honeydew;
            this.State_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.State_textBox.Location = new System.Drawing.Point(1188, 426);
            this.State_textBox.Name = "State_textBox";
            this.State_textBox.Size = new System.Drawing.Size(100, 26);
            this.State_textBox.TabIndex = 31;
            this.State_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.State_textBox.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(940, 429);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 18);
            this.label14.TabIndex = 30;
            this.label14.Text = "EWMA";
            this.label14.Visible = false;
            // 
            // EWMA_textBox
            // 
            this.EWMA_textBox.BackColor = System.Drawing.Color.Honeydew;
            this.EWMA_textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EWMA_textBox.Location = new System.Drawing.Point(1001, 426);
            this.EWMA_textBox.Name = "EWMA_textBox";
            this.EWMA_textBox.Size = new System.Drawing.Size(100, 26);
            this.EWMA_textBox.TabIndex = 29;
            this.EWMA_textBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.EWMA_textBox.Visible = false;
            // 
            // Px_chart
            // 
            chartArea1.Name = "ChartArea1";
            this.Px_chart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Px_chart.Legends.Add(legend1);
            this.Px_chart.Location = new System.Drawing.Point(12, 503);
            this.Px_chart.Name = "Px_chart";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.Name = "LastPrice";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.Aqua;
            series2.Enabled = false;
            series2.Legend = "Legend1";
            series2.Name = "EMWA";
            series2.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            this.Px_chart.Series.Add(series1);
            this.Px_chart.Series.Add(series2);
            this.Px_chart.Size = new System.Drawing.Size(782, 300);
            this.Px_chart.TabIndex = 5;
            this.Px_chart.Text = "chart1";
            // 
            // startAlgo_button
            // 
            this.startAlgo_button.BackColor = System.Drawing.Color.Red;
            this.startAlgo_button.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.startAlgo_button.Location = new System.Drawing.Point(134, 25);
            this.startAlgo_button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.startAlgo_button.Name = "startAlgo_button";
            this.startAlgo_button.Size = new System.Drawing.Size(112, 69);
            this.startAlgo_button.TabIndex = 33;
            this.startAlgo_button.Text = "Start Algo";
            this.startAlgo_button.UseVisualStyleBackColor = false;
            this.startAlgo_button.Click += new System.EventHandler(this.startAlgo_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(845, 701);
            this.Controls.Add(this.startAlgo_button);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.Message_textBox);
            this.Controls.Add(this.State_textBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.Px_chart);
            this.Controls.Add(this.EWMA_textBox);
            this.Controls.Add(this.Ticker_Info_groupBox);
            this.Controls.Add(this.Contract_groupBox);
            this.Controls.Add(this.Connect_button);
            this.Font = new System.Drawing.Font("Microsoft YaHei", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Yifei\'s IB API";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Contract_groupBox.ResumeLayout(false);
            this.Contract_groupBox.PerformLayout();
            this.Ticker_Info_groupBox.ResumeLayout(false);
            this.Ticker_Info_groupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Px_chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Connect_button;
        private System.Windows.Forms.TextBox Message_textBox;
        private System.Windows.Forms.GroupBox Contract_groupBox;
        private System.Windows.Forms.Button AddTicker_button;
        private System.Windows.Forms.TextBox Exchange_textBox;
        private System.Windows.Forms.TextBox Currency_textBox;
        private System.Windows.Forms.TextBox SecType_textBox;
        private System.Windows.Forms.TextBox Symbol_textBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Stop_Ticker_button;
        private System.Windows.Forms.GroupBox Ticker_Info_groupBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox LastQty_textBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox LastPx_textBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox AskQty_textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox AskPx_textBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox BidPx_textBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox BidQty_textBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Volume_textBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart Px_chart;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox State_textBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox EWMA_textBox;
        private System.Windows.Forms.Button startAlgo_button;
    }
}

