﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IBApi;
using System.Windows.Forms.DataVisualization.Charting;

namespace My_IB_DREAMSTARTUP_Project
{
    public partial class Form1 : Form
    {
        delegate void MessageHandlerDelegate(string message);
        delegate void PxAndSizeHandlerDelegate();

        private EWapperImp ibClient;
        public ExchangeGateway exchangegateway_;
        private bool isConnected = false;
        protected int currentTicker = 1;
        public const int TICK_ID_BASE = 10000;
        private bool firstEMA = true;
        private double previousEMA = 0;
        private double currentEMA = 0;
        private List<double> EMAlist = new List<double>();
        private double coefficient = 0.5;
        private bool firstTrade = true;
        public Random random = new Random();
        private int size;
        private double max, min;

        private Contract place_Contract;
        private Order place_Order;

        enum State
        {
            ABOVE,
            BELOW
        };

        State mystate;

        public Form1()
        {
            InitializeComponent();
            startAlgo_button.Enabled = false;
        }

        private void Connect_button_Click(object sender, EventArgs e)
        {
            if (!IsConnected)
            {
                this.exchangegateway_ = new ExchangeGateway();
                exchangegateway_.decisionEvent += exchangegateway__decisionEvent;
                this.ibClient = new EWapperImp(this,exchangegateway_);
                this.ibClient.MyLastPxUpdate += ibClient_MyLastPxUpdate;
                try
                {
                    this.ibClient.ClientSocket.eConnect("127.0.0.1", 7496, 0);
                }
                catch(Exception)
                {
                    MessageBox.Show("Please check your connection attributes");
                    System.Environment.Exit(-1);
                }
 
            }
            else
            {
                this.IsConnected = false;
                this.ibClient.ClientSocket.eDisconnect();
                exchangegateway_.startAlgo();
            }                     
        }

        void exchangegateway__decisionEvent(int decison)
        {
            place_Contract = new Contract();
            place_Contract.Symbol = Symbol_textBox.Text.ToString();
            place_Contract.SecType = "STK";
            place_Contract.Exchange = "SMART";
            place_Contract.Currency = "USD";

            place_Order = new Order();
            place_Order.OrderId = ibClient.NextOrderId;
            place_Order.OrderType = "MKT";
            place_Order.TotalQuantity = 5;
            place_Order.Tif = "DAY";

            if (decison == 0)
            {
                place_Order.Action = "BUY";
                ibClient.ClientSocket.placeOrder(place_Order.OrderId, place_Contract, place_Order);
                // buy
            }
            else if (decison == 1)
            {
                place_Order.Action = "SELL";
                ibClient.ClientSocket.placeOrder(place_Order.OrderId, place_Contract, place_Order);
                // sell
            }
            else
            {
                // hold
            }

            
        }

        private void ibClient_MyLastPxUpdate(double price)
        {
            if (this.firstEMA)
            {
                this.previousEMA = price;
                this.currentEMA = price;
                this.firstEMA = false;
                this.EMAlist.Add(previousEMA);               
            }
            this.currentEMA = this.coefficient * price + (1 - this.coefficient) * this.previousEMA;
            this.EMAlist.Add(currentEMA);

            // Set trading algorithm here...
            if (this.EMAlist.Count >= 10)
            {
                
                if (this.currentEMA > this.previousEMA && this.mystate == State.BELOW)
                {
                    //buy...
                    if(this.firstTrade)
                    {
                        size = random.Next(1, 10);
                        size *= 100;
                        firstTrade = false;
                        //Send MKT order (buy, size);
                    }
                    else
                    {
                        //close position first...
                        //Send MKT order(sell,size);
                        //new position...
                        size = random.Next(1, 10);
                        size *= 100;
                        //Send MKT order (long, size);
                    }
                       
                }
                if (currentEMA < previousEMA &&  mystate == State.ABOVE)
                {
                    //sell...
                    if (firstTrade)
                    {
                        size = random.Next(1, 10);
                        size *= 100;
                        firstTrade = false;
                        //Send MKT order (short, size);
                    }
                    else
                    {
                        //close position first...
                        //Send MKT order(buy,size);
                        //new position...
                        size = random.Next(1, 10);
                        size *= 100;
                        //Send MKT order (short, size);
                    }
                }              
            }

            if (previousEMA > currentEMA)
            {
                mystate = State.BELOW;
            }
            if (previousEMA < currentEMA)
            {
                mystate = State.ABOVE;
            }

            previousEMA = currentEMA;
        }

        public bool IsConnected
        {
            get { return isConnected; }
            set { isConnected = value; }
        }

        private void AddTicker_button_Click(object sender, EventArgs e)
        {
            Contract mycontract = GetMDContract();
            exchangegateway_.symbol_ = Symbol_textBox.Text;
            ibClient.ClientSocket.reqMktData(TICK_ID_BASE + (currentTicker++), mycontract, "", false, null);
            startAlgo_button.Enabled = true;
            startAlgo_button.BackColor = Color.Green;
        }

        private void Stop_Ticker_button_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < currentTicker; i++)
            {
                ibClient.ClientSocket.cancelMktData(TICK_ID_BASE + i);
            }               
        }
        private Contract GetMDContract()
        {
            Contract mycontract = new Contract();
            mycontract.Symbol = Symbol_textBox.Text;
            mycontract.SecType = SecType_textBox.Text;
            mycontract.Currency = Currency_textBox.Text;
            mycontract.Exchange = Exchange_textBox.Text;
            return mycontract;
        }
        public void HandleMessage(string message)
        {
            if (this.InvokeRequired)
            {
                MessageHandlerDelegate callback = new MessageHandlerDelegate(HandleMessage);
                this.Invoke(callback, new object[] { message });
            }
            else
            {
                this.Message_textBox.Text += message + Environment.NewLine;
                this.Message_textBox.SelectionStart = this.Message_textBox.TextLength;
                //scroll to the caret
                this.Message_textBox.ScrollToCaret();
            }

        }
        public void HandlePxAndSize()
        {
            if (this.InvokeRequired)
            {
                PxAndSizeHandlerDelegate callback = new PxAndSizeHandlerDelegate(HandlePxAndSize);
                this.Invoke(callback, new object[] {});
            }
            else
            {
                this.BidPx_textBox.Text = ibClient.BidPx.ToString();
                this.BidQty_textBox.Text = ibClient.BidSize.ToString();
                this.AskPx_textBox.Text = ibClient.AskPx.ToString();
                this.AskQty_textBox.Text = ibClient.AskSize.ToString();
                this.LastPx_textBox.Text = ibClient.LastPx.ToString();
                this.LastQty_textBox.Text = ibClient.LastSize.ToString();
                this.Volume_textBox.Text = ibClient.Volume.ToString();
                this.EWMA_textBox.Text = currentEMA.ToString();
                this.State_textBox.Text = mystate.ToString();
                this.drawChart(ibClient.LastPx);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Connect_button.Parent
        }
        public void changeConnButtonText(string text)
        {
            if (this.InvokeRequired)
            {
                MessageHandlerDelegate callback = new MessageHandlerDelegate(changeConnButtonText);
                this.Invoke(callback, new object[] {text});
            }
            else
            {
                this.Connect_button.Text = text;
            }
        }

        private void drawChart(double price)
        {
            if (price != 0)
            {
                Px_chart.Visible = true;

                Px_chart.Series[0].Points.AddXY(DateTime.Now.ToOADate(), price);
                Px_chart.Series[1].Points.AddXY(DateTime.Now.ToOADate(), currentEMA);

                max = Math.Ceiling(Px_chart.Series[0].Points.FindMaxByValue("Y1").YValues[0]);
                min = Math.Floor(Px_chart.Series[0].Points.FindMinByValue("Y1").YValues[0]);

                Px_chart.ChartAreas[0].AxisY.Maximum = max;
                Px_chart.ChartAreas[0].AxisY.Minimum = min;
                // Set series chart type
                Px_chart.Series[0].ChartType = SeriesChartType.Line;
                Px_chart.Series[1].ChartType = SeriesChartType.Line;
                // Set point labels
                Px_chart.Series[0].IsValueShownAsLabel = false;
                Px_chart.Series[1].IsValueShownAsLabel = false;
                // Enable X axis margin
                Px_chart.ChartAreas[0].AxisX.IsMarginVisible = true;
                //chart1.ChartAreas[0].AxisY.IsMarginVisible = true;
                // Show as 3D
                Px_chart.ChartAreas[0].Area3DStyle.Enable3D = false;
            }
        }

        private void startAlgo_button_Click(object sender, EventArgs e)
        {
            //test OE
            //place_Contract = new Contract();
            //place_Contract.Symbol = Symbol_textBox.Text.ToString();
            //place_Contract.SecType = "STK";
            //place_Contract.Exchange = "SMART";
            //place_Contract.Currency = "USD";

            //place_Order = new Order();
            //place_Order.OrderId = ibClient.NextOrderId;
            //place_Order.OrderType = "MKT";
            //place_Order.TotalQuantity = 5;
            //place_Order.Tif = "DAY";
            //place_Order.Action = "BUY";

            //ibClient.ClientSocket.placeOrder(place_Order.OrderId, place_Contract, place_Order);

            ibClient.updatingMaxMinAcc();
            //wait 1 sec 
            wait(1);
            exchangegateway_.startAlgo();
            startAlgo_button.Enabled = false;
            startAlgo_button.BackColor = Color.Gray;
        }

        private void wait(int x)
        {
            DateTime t = DateTime.Now;
            DateTime tf = DateTime.Now.AddSeconds(x);

            while (t < tf)
            {
                t = DateTime.Now;
            }

        }
    }
}
