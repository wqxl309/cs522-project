﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IBApi;

namespace My_IB_DREAMSTARTUP_Project
{
    public class ExchangeGateway
    {
        public delegate void DecisionHandlerDelegate(int decison);
        public event DecisionHandlerDelegate decisionEvent;

        public string symbol_;
        public double askPrice_;
        public double bidPrice_;
        public double lastPrice_;  //
        public int askSize_;
        public int bidSize_;
        public int lastSize_;
        public int volume_;    
        private Timer mytimer_Collector;
        private Timer mytimer_Predictor;
        //private int counter =0;
        public double accSize = 0 ;  //
        public double maxLastprice = 0;  //
        public double minLastPrice = 0;  //
        //double[,] XY;
        
        ////////////////////////////////////////////////////

        private bool firsttime = true;
        private bool isPredictorStart = false;
        alglib.decisionforest model = null;
        public int trainingDataLength = 100; // ALGlib Dforest input data length
        public List<double[]> trainingDataSet = new List<double[]>(); 

        // Intermediate variables for update indicators
        private double EMAlOld = new double(); //DIF=EMAs - EMAl
        private double EMAsOld = new double();
        private double DEAOld = new double();
        private double mean_GainOld1 = new double();
        private double mean_LossOld1 = new double();
        private double mean_GainOld2 = new double();
        private double mean_LossOld2 = new double();
        private double mean_GainOld3 = new double();
        private double mean_LossOld3 = new double();
        private double price_old = new double();

        public enum MarketState
        {
            UP = 1,
            UpNEUTRAL = 2,
            DOWN = 3,
            DownNEUTRAL = 4
        }

        private MarketState _currentmarketstate = MarketState.UpNEUTRAL;
                
              
        public void startAlgo()
        {           
            mytimer_Collector = new Timer();
            mytimer_Collector.Tick += new EventHandler(mytimer_event);
            mytimer_Collector.Interval = 10 * 1000; // will be put on UI
            mytimer_Collector.Enabled = true;
            //counter++;
        }

        private void mytimer_event(object sender, EventArgs e)   //where training happen
        {
            if (firsttime)
            {
                firsttime = false;
            }
            else
            {
                // Collect training data
                double[] Tickdata = new double[4] { (lastPrice_), minLastPrice, maxLastprice, accSize };
                trainingDataSet.Add(Tickdata);
               
                if (trainingDataSet.Count == trainingDataLength)
                {
                    // ALG-training and reveive model, assign model to the variable "alglib.decisionforest model" 
                    ProcessHistoricalData();
                    // model trained
                    if (isPredictorStart == false)
                    {
                        startPredictor();
                    }
                    trainingDataSet.Clear();
                }
            }
        }

        public void stopCollector()
        {
            mytimer_Collector.Enabled = false;
            mytimer_Predictor.Enabled = false;
        }

        public void startPredictor()
        {
            isPredictorStart = true;
            mytimer_Predictor = new Timer();
            mytimer_Predictor.Interval = 10 * 1000;
            mytimer_Predictor.Tick += mytimer_Predictor_Tick;
            mytimer_Predictor.Enabled = true;
        }
        //
        private void mytimer_Predictor_Tick(object sender, EventArgs e)    // where update/ predict happens
        {
            if (model != null)
            {
                //Predict and return "BUY"/"SELL" decisions
                int predicted;  //0 up trend , 1 down trend
                double[] trading_data = {lastPrice_, minLastPrice, maxLastprice, accSize};
                UpdateStrategy(trading_data, out predicted);

                MarketState lastState = _currentmarketstate;

                switch (lastState)
                {
                    case MarketState.UP:
                        if (predicted == 1) 
                        { 
                            _currentmarketstate = MarketState.UpNEUTRAL;
                            decisionEvent(2);
                            break; 
                        }
                        else if (predicted == 0) 
                        { 
                            _currentmarketstate = MarketState.DOWN;
                            decisionEvent(1);
                            break; 
                        }
                        break;
                    case MarketState.UpNEUTRAL:
                        if (predicted == 1) 
                        { 
                            _currentmarketstate = MarketState.UpNEUTRAL;
                            decisionEvent(2);
                            break; 
                        }
                        else if (predicted == 0) 
                        { 
                            _currentmarketstate = MarketState.DOWN;
                            decisionEvent(1);
                            break; 
                        }
                        break;
                    case MarketState.DOWN:
                        if (predicted == 1) 
                        { 
                            _currentmarketstate = MarketState.UP;
                            decisionEvent(0);
                            break; 
                        }
                        else if (predicted == 0) 
                        { 
                            _currentmarketstate = MarketState.DownNEUTRAL;
                            decisionEvent(2);
                            break; 
                        }
                        break;
                    case MarketState.DownNEUTRAL:
                        if (predicted == 1) 
                        { 
                            _currentmarketstate = MarketState.UP;
                            decisionEvent(0);
                            break; 
                        }
                        else if (predicted == 0) 
                        { 
                            _currentmarketstate = MarketState.DownNEUTRAL;
                            decisionEvent(2);
                            break; 
                        }
                        break;
                }                
            }            
        }
        
        
        
        //////////////////// data processing, training, trading functions  ////////////////////
        
        public void ProcessHistoricalData()  //last, min, max , size
        {
            //int length = tradeData.Count;
            int length = 2000;
            //string[] time = new string[length];
            double[] price = new double[length];
            double[] volume = new double[length];
            double[] min = new double[length];
            double[] max = new double[length];

            int i = 0;
            foreach (double[] trade in trainingDataSet)
            {
                //time[i] = trade.Time;
                price[i] = trade[0];
                min[i]=trade[1];
                min[i] = trade[2];
                volume[i] = trade[3];
                if (i >= length - 1) break;
                i++;
            }

            // costruct indicators
            //double[] MACDc; direct compute by DIF-DEA
            double[] DIFc;
            double[] DEAc;
            double[] RSI1c;
            double[] RSI2c;
            double[] RSI3c;
            //double[] FIc;                

            Fun_MACD(3, 7, 5, ref price, out DIFc, out DEAc, out EMAlOld, out EMAsOld);
            Fun_RSI(3, ref price, out RSI1c, out mean_GainOld1, out mean_LossOld1);
            Fun_RSI(5, ref price, out RSI2c, out mean_GainOld2, out mean_LossOld2);
            Fun_RSI(7, ref price, out RSI3c, out mean_GainOld3, out mean_LossOld3);
            //Fun_FI(5, ref price, ref volume, out FIc);

            // update intermediate variables
            DEAOld = DEAc[DEAc.Length - 1];
            price_old = price[price.Length - 1];

            List<int> indices = new List<int>();
            List<double> slopes = new List<double>();
            double threshold = 0.01;
            zigzag(threshold, ref price, out indices, out slopes);
            indices.ToArray();
            slopes.ToArray();

            // discretization and mismatch          
            // traning data is filled backwardly
            // order of predictors RSI1,RSI2,RSI3,MACD,FI
            int drop = 10;
            int mis_period = 1;
            int length_drop = length - drop;
            double[,] proData = new double[length - drop - mis_period, 5];// used to contain the "discrete" results

            int index = indices.Count - 1;
            for (int j = 1; j <= length - drop - mis_period; ++j)
            {
                // response discretization, only applied when training
                for (; index > 0; --index)
                {
                    if (length - j >= indices[index])
                    {
                        break;
                    }
                }

                if (slopes[index] > 0) proData[length - drop - mis_period - j, 4] = 0;
                else proData[length - drop - mis_period - j, 4] = 1;

                // predictors discritization
                if (RSI1c[length - mis_period - j] <= 20) proData[length - mis_period - drop - j, 0] = 0;
                else if (RSI1c[length - mis_period - j] > 20 && RSI1c[length - mis_period - j] <= 40) proData[length - mis_period - drop - j, 0] = 1;
                else if (RSI1c[length - mis_period - j] > 40 && RSI1c[length - mis_period - j] <= 60) proData[length - mis_period - drop - j, 0] = 2;
                else if (RSI1c[length - mis_period - j] > 60 && RSI1c[length - mis_period - j] <= 80) proData[length - mis_period - drop - j, 0] = 3;
                else proData[length - mis_period - drop - j, 0] = 4;

                if (RSI2c[length - mis_period - j] <= 20) proData[length - mis_period - drop - j, 1] = 0;
                else if (RSI2c[length - mis_period - j] > 20 && RSI2c[length - mis_period - j] <= 40) proData[length - mis_period - drop - j, 1] = 1;
                else if (RSI2c[length - mis_period - j] > 40 && RSI2c[length - mis_period - j] <= 60) proData[length - mis_period - drop - j, 1] = 2;
                else if (RSI2c[length - mis_period - j] > 60 && RSI2c[length - mis_period - j] <= 80) proData[length - mis_period - drop - j, 1] = 3;
                else proData[length - mis_period - drop - j, 1] = 4;

                if (RSI3c[length - mis_period - j] <= 20) proData[length - mis_period - drop - j, 2] = 0;
                else if (RSI3c[length - mis_period - j] > 20 && RSI3c[length - mis_period - j] <= 40) proData[length - mis_period - drop - j, 2] = 1;
                else if (RSI3c[length - mis_period - j] > 40 && RSI3c[length - mis_period - j] <= 60) proData[length - mis_period - drop - j, 2] = 2;
                else if (RSI3c[length - mis_period - j] > 60 && RSI3c[length - mis_period - j] <= 80) proData[length - mis_period - drop - j, 2] = 3;
                else proData[length - mis_period - drop - j, 2] = 4;

                if (DIFc[length - mis_period - j] - DEAc[length - mis_period - j] > 0 & DIFc[length - mis_period - j] < 0 & DEAc[length - mis_period - j] < 0) proData[length - mis_period - drop - j, 3] = 0;
                else if (DIFc[length - mis_period - j] - DEAc[length - mis_period - j] < 0 & DIFc[length - mis_period - j] > 0 & DEAc[length - mis_period - j] > 0) proData[length - mis_period - drop - j, 3] = 1;
                else proData[length - mis_period - drop - j, 3] = 2;

                //double FI1 = quantile(0.2,ref FIc);
                //double FI2 = quantile(0.4,ref FIc);
                //double FI3 = quantile(0.6,ref FIc);
                //double FI4 = quantile(0.8,ref FIc);
                //if (FIc[length - mis_period - j] <= FI1) proData[length - mis_period - drop - j,5] = 0;
                //else if (FIc[length - mis_period - j] > FI1 && FIc[length - mis_period - j] <= FI2) proData[length - mis_period - drop - j, 5] = 1;
                //else if (FIc[length - mis_period - j] > FI2 && FIc[length - mis_period - j] <= FI3) proData[length - mis_period - drop - j, 5] = 2;
                //else if (FIc[length - mis_period - j] > FI3 && FIc[length - mis_period - j] <= FI4) proData[length - mis_period - drop - j, 5] = 3;
                //else proData[length - mis_period - drop - j, 5] = 4;
            }
            // train and update RF classifier
            TrainStartegy(proData);
        }

        private void TrainStartegy(double[,] training_data)
        {
            int input_num = training_data.GetLength(1);
            int data_num = training_data.GetLength(0);
            int info;            
            alglib.dfreport dfrep;
            // fit the forest    
            alglib.dfbuildrandomdecisionforest(training_data, data_num, input_num - 1, 2, 100, 0.6, out info, out model, out dfrep);

            // chech error
            if (info == -2)
            {
                int i = 0;
            }
            if (info == -1)
            {
                int i = 0;
            } 
        }


        private void UpdateStrategy(double[] trading_data, out int predicted)
        {
            // Intermediate variables for update indicators
            double EMAlNew = new double(); //DIF=EMAs - EMAl
            double EMAsNew = new double();
            double DEANew = new double();
            double mean_GainNew1 = new double();
            double mean_LossNew1 = new double();
            double mean_GainNew2 = new double();
            double mean_LossNew2 = new double();
            double mean_GainNew3 = new double();
            double mean_LossNew3 = new double();
            double RSI1 = new double();
            double RSI2 = new double();
            double RSI3 = new double();

            // update indicators  

            //public static void alglib.dfprocess(decisionforest df, double[] x, ref double[] y)
            Update_MACD(3, 7, 5, trading_data[0], DEAOld, EMAlOld, EMAlOld, out DEANew, out EMAsNew, out EMAlNew);

            double[] Price_update = { price_old, trading_data[0] };

            Update_RSI(3, Price_update, mean_GainOld1, mean_LossOld1, out RSI1, out mean_GainNew1, out mean_LossNew1);
            Update_RSI(5, Price_update, mean_GainOld2, mean_LossOld2, out RSI2, out mean_GainNew2, out mean_LossNew2);
            Update_RSI(7, Price_update, mean_GainOld3, mean_LossOld3, out RSI3, out mean_GainNew3, out mean_LossNew3);

            //update old intermedia values
            price_old = trading_data[0];
            EMAlOld = EMAlNew; //DIF=EMAs - EMAl
            EMAsOld = EMAsNew;
            mean_GainOld1 = mean_GainNew1;
            mean_GainOld2 = mean_GainNew2;
            mean_GainOld3 = mean_GainNew3;
            mean_LossOld1 = mean_LossNew1;
            mean_LossOld2 = mean_LossNew2;
            mean_LossOld3 = mean_LossNew3;

            // order of the indicators RSI1,RSI2,RSI3,MACD_DIF,MACD_DEA,//FI
            // order of the discrete output  RSI1,RSI2,RSI3,MACD,//FI
            double[] indicators = { RSI1, RSI2, RSI3, EMAsNew - EMAlNew, DEANew };
            double[] trading_discrete = new double[4];
            discret_input(indicators, out trading_discrete);

            // predict trading resultud
            double[] predict = new double[2]; //posterior probabilities

            try
            {
                alglib.dfprocess(model, trading_discrete, ref predict);
            }
            catch (Exception e)
            {
                int test = 0;
            }

            if (predict[0] > predict[1])
            {
                predicted = 0;
            }
            else
            {
                predicted = 1;
            }
        }


        ////////////////////   help functions  //////////////////////////////////

        private void Fun_MACD(double Short, double Long, double dea, ref double[] prices,
                                out double[] DIF, out double[] DEA, out double EMAl, out double EMAs)
        {    // length MACD =length prices   
            //denominaors and numerators
            int n = prices.Length;
            double Short_a = 2.0 / (Short + 1);
            double Short_b = 1.0 - Short_a;
            double Long_a = 2.0 / (Long + 1);
            double Long_b = 1.0 - Long_a;
            double dea_a = 2.0 / (dea + 1);
            double dea_b = 1.0 - dea_a;
            EMAl = prices[1];
            EMAs = prices[1];

            DEA = new double[n];
            DIF = new double[n];
            DEA[0] = EMAs - EMAl;
            DIF[0] = EMAs - EMAl;

            for (int i = 1; i < n; ++i)
            {
                EMAs = prices[i] * Short_a + EMAs * Short_b;
                EMAl = prices[i] * Long_a + EMAl * Long_b;
                DIF[i] = EMAs - EMAl;
                DEA[i] = DIF[i] * dea_a + DEA[i - 1] * dea_b;
            }
        }

        private void Update_MACD(double Short, double Long, double dea, double price, double DEAOld, double EMAsOld, double EMAlOld,
                                 out double DEA, out double EMAsNew, out double EMAlNew)
        {
            double Short_a = 2.0 / (Short + 1);
            double Short_b = 1.0 - Short_a;
            double Long_a = 2.0 / (Long + 1);
            double Long_b = 1.0 - Long_a;
            double dea_a = 2.0 / (dea + 1);
            double dea_b = 1.0 - dea_a;

            EMAsNew = price * Short_a + EMAsOld * Short_b;
            EMAlNew = price * Long_a + EMAlOld * Long_b;
            //DIF = EMAsNew - EMAlNew;
            //DEA = DIF*dea_a + DEAOld*dea_b;
            DEA = (EMAsNew - EMAlNew) * dea_a + DEAOld * dea_b;
        }

        private void Fun_RSI(int period, ref double[] prices, out double[] RSI, out double mean_Gain, out double mean_Loss)
        {
            int n = prices.Length;
            RSI = new double[n];
            double[] PandL = new double[n];
            mean_Gain = 0;
            mean_Loss = 0;
            int num_Gain = 0;
            int num_Loss = 0;

            PandL[0] = 0;
            RSI[0] = 0;
            for (int i = 1; i < period; ++i)
            {
                PandL[i] = prices[i] - prices[i - 1];
                if (PandL[i] >= 0)
                {
                    num_Gain += 1;
                    mean_Gain += PandL[i];
                }
                else
                {
                    num_Loss += 1;
                    mean_Loss += PandL[i];
                }
                RSI[i] = 0;
            }
            mean_Gain = mean_Gain / num_Gain;
            mean_Loss = -mean_Loss / num_Loss;
            RSI[period] = 100 * mean_Gain / (mean_Gain + mean_Loss);

            double na = 2.0 / (period + 1);
            double nb = 1.0 - na;
            for (int i = period; i < n; ++i)
            {
                PandL[i] = prices[i] - prices[i - 1];
                mean_Gain = (PandL[i] > 0 ? PandL[i] : 0) * na + mean_Gain * nb;
                mean_Loss = -(PandL[i] < 0 ? PandL[i] : 0) * na + mean_Loss * nb;
                RSI[i] = 100 * mean_Gain / (mean_Gain + mean_Loss);
            }
        }

        private void Update_RSI(int period, double[] prices, double mean_GainOld, double mean_LossOld,
                                out double RSI, out double mean_GainNew, out double mean_LossNew)
        {
            double na = 2.0 / (period + 1);
            double nb = 1.0 - na;
            double PandL = prices[1] - prices[0];
            mean_GainNew = (PandL > 0 ? PandL : 0) * na + mean_GainOld * nb;
            mean_LossNew = -(PandL < 0 ? PandL : 0) * na + mean_LossOld * nb;
            RSI = 100 * mean_GainNew / (mean_GainNew + mean_LossNew);
        }

        private void Fun_FI(int period, ref double[] prices, ref double[] volumes, out double[] FI)
        {
            double na = 2.0 / (period + 1);
            double nb = 1.0 - na;
            int n = prices.Length;
            FI = new double[n];
            FI[0] = 0;

            for (int i = 1; i < n; ++i)
            {
                FI[i] = na * (prices[i] - prices[i - 1]) * volumes[i] + nb * FI[i - 1];
            }
        }

        // zigzag function for response
        private void zigzag(double threshold, ref double[] prices, out List<int> indices, out List<double> slopes)
        {
            int n = prices.Length;
            int start_index = 0;
            indices = new List<int>();
            slopes = new List<double>();

            while (start_index < n)
            {
                int current_index = start_index;
                for (current_index = start_index; current_index < n - 1; ++current_index)
                {
                    if (Math.Abs((prices[current_index] - prices[start_index]) / prices[start_index]) >= threshold)
                    {
                        if (Math.Abs(prices[current_index + 1] - prices[start_index]) < Math.Abs(prices[current_index] - prices[start_index]))
                        {
                            indices.Add(current_index);
                            slopes.Add((prices[current_index] - prices[start_index]) / prices[start_index]);
                            start_index = current_index;
                            break;
                        }
                    }
                }
                // deal with end of the array
                if (current_index == n - 1)
                {
                    indices.Add(current_index);
                    slopes.Add((prices[current_index] - prices[start_index]) / prices[start_index]);
                    break;
                }

                else if (current_index == n - 2 && current_index == start_index)
                {
                    if (Math.Abs(prices[n - 1] - prices[n - 2]) <= 0.0001) // last two price very close
                    {
                        indices[indices.Count] = n - 1; //replace the existing last index, n-2 to n-1
                        slopes[slopes.Count] = (prices[n - 1] - prices[indices[indices.Count - 1]]) / prices[indices[indices.Count - 1]];
                        break;
                    }
                    else   // last two price has a non-negligible difference
                    {
                        indices.Add(n - 1);
                        slopes.Add((prices[n - 1] - prices[n - 2]) / prices[n - 2]);
                        break;
                    }
                }
            }
        }

        // quantile function compute a single quantile
        private double quantile(double quant, ref double[] values)
        {
            if (quant > 1)
            {
                quant = 1;
            }
            else if (quant < 0)
            {
                quant = 0;
            }
            int num = values.Length;
            int q = (int)(Math.Ceiling(num * quant));
            return values[q - 1];
        }

        // discretize a single input or one data point, for trading
        // order of the indicators RSI1,RSI2,RSI3,MACD_DIF,MACD_DEA,FI
        // order of the discrete output  RSI1,RSI2,RSI3,MACD,FI

        private void discret_input(double[] indicators, out double[] discrete)
        {
            discrete = new double[4];

            if (indicators[0] <= 20) discrete[0] = 0;
            else if (indicators[0] > 20 && indicators[0] <= 40) discrete[0] = 1;
            else if (indicators[0] > 40 && indicators[0] <= 60) discrete[0] = 2;
            else if (indicators[0] > 60 && indicators[0] <= 80) discrete[0] = 3;
            else discrete[0] = 4;

            if (indicators[1] <= 20) discrete[1] = 0;
            else if (indicators[1] > 20 && indicators[1] <= 40) discrete[1] = 1;
            else if (indicators[1] > 40 && indicators[1] <= 60) discrete[1] = 2;
            else if (indicators[1] > 60 && indicators[1] <= 80) discrete[1] = 3;
            else discrete[1] = 4;

            if (indicators[2] <= 20) discrete[2] = 0;
            else if (indicators[2] > 20 && indicators[2] <= 40) discrete[2] = 1;
            else if (indicators[2] > 40 && indicators[2] <= 60) discrete[2] = 2;
            else if (indicators[2] > 60 && indicators[2] <= 80) discrete[2] = 3;
            else discrete[2] = 4;

            if (indicators[3] - indicators[4] > 0 & indicators[3] < 0 & indicators[4] < 0) discrete[3] = 0;
            else if (indicators[3] - indicators[4] < 0 & indicators[3] > 0 & indicators[4] > 0) discrete[3] = 1;
            else discrete[3] = 2;
        }
    }    
}
