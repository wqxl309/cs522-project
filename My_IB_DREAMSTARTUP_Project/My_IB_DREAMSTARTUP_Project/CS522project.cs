﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace My_IB_DREAMSTARTUP_Project
{
    public partial class CS522project : Form
    {
        public CS522project()
        {
            InitializeComponent();
        }
        private EWapperImp ibClient;
        private bool isConnected = false;


        private void Connect_button_Click(object sender, EventArgs e)
        {
            if (!isConnected)
            {
                this.ibClient = new EWapperImp(this);
                try
                {
                    this.ibClient.ClientSocket.eConnect("127.0.0.1", 7496, 0);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please check your connection attributes");
                    System.Environment.Exit(-1);
                }
            }        
            else
            {
                this.isConnected = false;
                this.ibClient.ClientSocket.eDisconnect();
            }
        }
    }
}
